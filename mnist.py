import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn.datasets as data
sns.set_context('poster')
sns.set_style('white')
sns.set_color_codes()
plot_kwds = {'alpha' : 0.5, 's' : 80, 'linewidths':0}
from sklearn.datasets import fetch_openml
mnist = fetch_openml('mnist_784', version=1)


X, y = mnist["data"], mnist["target"]



from sklearn.manifold import TSNE
import hdbscan


projection = TSNE(n_iter = 1500,perplexity = 30).fit_transform(X)
plt.scatter(*projection.T,**plot_kwds)
plt.show()

